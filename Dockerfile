FROM node:14.17.0-alpine3.13 as build

# Create app directory
WORKDIR /app

COPY package*.json /app/

# Install app dependencies
RUN npm install

COPY . /app/

FROM nginx

COPY --from=build /app/ /usr/share/nginx/html

# COPY ./default.conf /etc/nginx/conf.d/default.conf