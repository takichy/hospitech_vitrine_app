## Repo pour le site vitrine de hospitech

# To run this project you need to install nodeJS and npm on your localhost
-   nodeJS
-   npm

# after that you should run this command on your terminal to start the project:
-   node index.js

# Docker : 
-   pour le fichier de configuration docker ce n'est pas pour le test on local, c'est pour l'env prod.

# DNS
-   vous pouvez voir le projet sur l'URL [www.hospitech.fr](www.hospitech.fr) , une fois vous avez pusher le code sur la branche main.
